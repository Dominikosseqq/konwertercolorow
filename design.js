const hamburger = document.querySelector(".hamburger");
const singleNavigation = document.querySelector(".single-navigation");

hamburger.addEventListener("click", () => {
    singleNavigation.classList.toggle("single-navigation--on");
});
